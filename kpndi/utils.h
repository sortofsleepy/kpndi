#pragma once

#include "Processing.NDI.Lib.h"
#include <chrono>

using namespace std::chrono;

namespace kp { namespace ndi {
    
    // check to try to initlaize the NDI library 
    static void ndi_init(){
        // try to initialize library
        if (!NDIlib_initialize()){
            #ifdef ANDROID 
                LOG_E("Unable to initialize NDI");
                abort();
            #endif 
        }

    }

    // Finds NDI sources
    static void find_sources(int searchTime = 5000){
        NDIlib_find_instance_t pNDI_find = NDIlib_find_create_v2();
        if (!pNDI_find){
            #ifdef ANDROID
                LOG_E("Unable to initilize NDI finder");
                abort();
            #endif 
        }
        const auto start = high_resolution_clock::now();
        
        // Get the updated list of sources
		uint32_t no_sources = 0;

        for(start; high_resolution_clock::now() - start < minutes(1);){
            if (!NDIlib_find_wait_for_sources(pNDI_find, 5000/* 5 seconds */))
			{
#ifdef ANDROID
                LOG_E("No change to the sources found.\n"); continue;
#endif
			}

          
		    const NDIlib_source_t* p_sources = NDIlib_find_get_current_sources(pNDI_find, &no_sources);
			// Display all the sources.
#ifdef ANDROID
            LOG_I("Network sources (%u found).\n", no_sources);
#endif
		    for (uint32_t i = 0; i < no_sources; i++)
			    LOG_I("%u. %s\n", i + 1, p_sources[i].p_ndi_name);
        }

        if(no_sources == 0){
#ifdef ANDROID
            LOG_E("NO SOURCES FOUND");
            abort();
#endif
        }
    }



}}

/*
 *  // somewhat works, adapted from
    // https://blog.minhazav.dev/how-to-use-renderscript-to-convert-YUV_420_888-yuv-image-to-bitmap/
    static void yuvToRgb(int image_width, int image_height, const uint8_t* y_buffer,
                  const uint8_t* u_buffer, const uint8_t* v_buffer, int y_pixel_stride,
                  int uv_pixel_stride, int y_row_stride, int uv_row_stride,
                  uint8_t *nv21){

        // Copy Y channel.
        for(int y = 0; y < image_height; ++y) {
            int destOffset = image_width * y;
            int yOffset = y * y_row_stride;
            memcpy(nv21 + destOffset, y_buffer + yOffset, image_width);
        }
        // Copy UV Channel.
        int idUV = image_width * image_height;
        int uv_width = image_width / 2;
        int uv_height = image_height / 2;

        for(int y = 0; y < uv_height; ++y) {
            int uvOffset = y * uv_row_stride;

            for (int x = 0; x < uv_width; ++x) {
                int bufferIndex = uvOffset + (x * uv_pixel_stride);

                // V channel.
                nv21[idUV++] = v_buffer[bufferIndex];

                // U channel.
                nv21[idUV++] = u_buffer[bufferIndex];

                // push it further one more step
                //idUV++;
            }
        }
    }



    // adapted from
    //https://stackoverflow.com/questions/56037629/arcore-how-to-convert-yuv-camera-frame-to-rgb-frame-in-c
    static void yuv2rgb(uint8_t yValue, uint8_t uValue, uint8_t vValue,
        uint8_t *r, uint8_t *g, uint8_t *b)  {

        int rTmp = yValue + (1.370705 * (vValue-128));
        // or fast integer computing with a small approximation
        // rTmp = yValue + (351*(vValue-128))>>8;
        int gTmp = yValue - (0.698001 * (vValue-128)) - (0.337633 * (uValue-128));
        // gTmp = yValue - (179*(vValue-128) + 86*(uValue-128))>>8;
        int bTmp = yValue + (1.732446 * (uValue-128));

        // bTmp = yValue + (443*(uValue-128))>>8;
        *r = std::clamp(rTmp, 0, 255);
        *g = std::clamp(gTmp, 0, 255);
        *b = std::clamp(bTmp, 0, 255);
    }


    static void toRGB(int image_width, int image_height, const uint8_t* y_buffer,
                  const uint8_t* u_buffer, const uint8_t* v_buffer, int y_pixel_stride,
                  int uv_pixel_stride, int y_row_stride, int uv_row_stride,
                  uint8_t *nv21){
        int uv_width = image_width / 2;
        int uv_height = image_height / 2;

        for(int y = 0; y < uv_height; y++) {
            int uvOffset = y * uv_row_stride;
            uint8_t* p_image = (uint8_t*)nv21 + (image_height * 4) * y;

            for (int x = 0; x < uv_width; x++, p_image += 4) {
                int bufferIndex = uvOffset + (x * uv_pixel_stride);

                // y channel
                uint8_t y = y_buffer[bufferIndex];

                 // U channel.
                uint8_t u = u_buffer[bufferIndex];

                // V channel.
                uint8_t v =  v_buffer[bufferIndex];

                uint8_t r,g,b;
                kp::ndi::yuv2rgb(y,u,v,&r,&g,&b);


                p_image[0] = b;
                p_image[1] = g;
                p_image[2] = r;
                p_image[3] = 1;

            }
        }
    }

 */