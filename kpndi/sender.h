#pragma once

#include <string>
#include "Processing.NDI.Lib.h"
#include "kpndi/types.h"

namespace kp { namespace ndi {

    typedef std::shared_ptr<class NDISender>NDISenderRef;

    class NDISender{
        public:
           
            NDISender(const char* name){
                senderDescripton.p_ndi_name = name;
            }

            static NDISenderRef create(std::string senderName="Kanpai NDI"){
               return NDISenderRef(new NDISender(senderName.c_str()));
            }   

            // initializes things and starts polling for possible connections.
            void setup(){
               
                sender = NDIlib_send_create(&senderDescripton);
	            if (!sender){

#ifdef ANDROID
                    LOG_E("Can't create NDI sender : ");
	                abort();
#endif
	            }
            }

            /**
             * Sends a video frame
             * @param frame {kp::ndi::VideoFrameRef} a video frame to send.
             */
            void send_frame(kp::ndi::VideoFrameRef frame){

                // ensure frame has memory allocated and is not updating.
                if(frame->hasData() && !frame->isUpdating()){
                    NDIlib_send_send_video_v2(sender, &frame->getNDIFrame());
                }else{

#ifdef ANDROID
                    LOG_I("NDI Sender attempting to send frame but no frame data");
#endif
                }
            }

            /**
             * Polls to discover any attempts to connect to the device running the code.
             * @return
             */
            // TODO allow loop check to be custom
            bool pollConections(){
                 bool connectionFound = false;
                for(int idx = 0; idx < 1000; ++idx){
                    if (!NDIlib_send_get_no_connections(sender, 1000)){
#ifdef ANDROID
                        // Display status
                        LOG_I("No current connections, so no rendering needed (%d).\n", idx);
#endif
                    }else{
                        connectionFound = true;
                        break;
                    }
                }

                return connectionFound;
            }

            /**
             * Allows you to add metadata that you want to send to the receiver.
             * @param description {char*}
             */
            void addConnectionMetadata(char * description){
                senderMetadata.p_data = description;
                NDIlib_send_add_connection_metadata(sender, &senderMetadata);
            }

        protected:
            NDIlib_send_instance_t sender;
            NDIlib_send_create_t senderDescripton; 
            NDIlib_metadata_frame_t senderMetadata;

    };

}}