#pragma once 
#include <memory>
#include "Processing.NDI.Lib.h"
#include "kpndi/utils.h"

namespace kp { namespace ndi {

    typedef std::shared_ptr<class VideoFrame>VideoFrameRef;
    class VideoFrame {
        public:
            VideoFrame(int width = 1920, int height = 1080){
                // set defaults
                videoFrame.xres = frame_width;
                videoFrame.yres = frame_height;
                videoFrame.FourCC = NDIlib_FourCC_type_BGRX;
                videoFrame.p_data = nullptr;
                videoFrame.line_stride_in_bytes = strideInBytes;
            }

            static VideoFrameRef create() {
                return VideoFrameRef(new VideoFrame);
            }

            static VideoFrameRef create(int width,int height) {
                return VideoFrameRef(new VideoFrame(width,height));
            }

            /**
             * Updates frame information including width,height, and stride
             * @param width {number} the width of the frame.
             * @param height {number} the height of the frame.
             */
            void updateFrameInfo(int width,int height){
                updatingFrame = true;
                videoFrame.xres = width;
                videoFrame.yres = height;

                // set the stride
                setStride(width);

                // (re)allocate memory for the data that will go into the frame.
                allocateFrameMemory();
            }

            /**
             * Sets the stride for the video frame
             * @param count {int} the value used to calculate the stride. Formula appears to be <count> + number of channels
             */
            void setStride(int count){
                strideInBytes = count * channelCount;
                videoFrame.line_stride_in_bytes = strideInBytes;
            }

            /**
             * Returns whether or not there is pixel information associated with the frame.
             * @return
             */
            bool hasData(){
                return videoFrame.p_data != nullptr;
            }

            /**
             * Used to check if the frame might be re-allocating memory
             * @return
             */
            bool isUpdating(){
                return updatingFrame;
            }

            /**
             * Allocates memory for the frame.
             */
            void allocateFrameMemory(){

                // free old memory
                if(videoFrame.p_data != nullptr){
                    free(videoFrame.p_data);
                }

                // re-allocate memory for frame. 
                videoFrame.p_data = (uint8_t*)malloc(videoFrame.xres * videoFrame.yres * channelCount);

                updatingFrame = false;
            }

            /**
             * Update frame data
             * @param pixels {uint8_t *} pixel information for the frame.
             */
            void updatePixels(uint8_t * pixels){
                videoFrame.p_data = pixels;
            }

            /**
             * Returns the NDI frame object.
             * @return
             */
            NDIlib_video_frame_v2_t& getNDIFrame(){
                return videoFrame;
            }

        protected:
            int frame_width = 1920;
            int frame_height = 1080;

            bool updatingFrame;

            // frame that will get sent to receiver
            // TODO not sure if it's best to keep one frame or have multiple. 
            NDIlib_video_frame_v2_t videoFrame;
            
            int channelCount = 4;
            int strideInBytes = frame_width * channelCount;

    };

