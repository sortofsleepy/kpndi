# KP NDI

A small wrapper around the NDI sdk. Currently tested in Android, but from what I've seen, the code is similar enough that this should work on other OS's as well. 

Current status 
====
* Video is working and I'm able to send frames to a receiver. 

TODO 
===
* Implement audio sending
* Implement receiving aspects as well possibly. 

Install 
===
Library is header only so you should be able to drop this into your project fairly easily. 

Installing NDK sdk
===
* https://www.ndi.tv/sdk/ to grab the libraries for your particular platform.
* If you want debugging, make sure to add the appropriate preprocessor definition 
  * `ANDROID` to trigger logging in Android
